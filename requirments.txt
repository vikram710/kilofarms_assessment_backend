Django==3.0.4
django-cors-headers==3.7.0
mysqlclient==2.0.3
python-dotenv==0.15.0
PyJWT==1.7.1