from django.db import models

PRODUCT_CATEGORY = (
    ('A', 'A'),
    ('A', 'B'),
)
class Product(models.Model):
    sku_name = models.CharField(max_length=100)
    sku_price = models.IntegerField(default=0)
    sku_category = models.CharField(
        max_length=2,
        choices=PRODUCT_CATEGORY,
        default="A",
    )
    created_at = models.DateField(auto_now_add=True)

    class Meta:
        db_table = 'products'
