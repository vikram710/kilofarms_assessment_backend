# Local Installation
- Clone the repo in a directory
	`git clone https://vikram710@bitbucket.org/vikram710/kiloframs_assessment_backend.git`
- Go to the directory
	`cd kiloframs_assessment_backend`
- Create a virtualenv
	`pip install virtualenv`
	`virtualenv .`
- Activate the virtualenv
	`.\Scripts\activate`
- Install all modules and packages
	`pip install -r requirements.txt`
- make a copy of `.env.example` file to `.env`
	`copy .env.example .env`
- Edit the values in the newly created `.env` file
- Run the following commands
	```
	manage.py makemigrations
	manage.py migrate
	manage.py runserver
	```