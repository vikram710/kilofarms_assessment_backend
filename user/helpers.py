import re
from datetime import datetime, date


def validate_login_data(data):
    error = ""
    isValid = True
    email_regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    if not data.get('email'):
        error = "Email field empty"
        isValid = False
    elif not data.get('password'):
        error = "Password field empty"
        isValid = False
    elif not re.search(email_regex, data.get('email')):
        error = "Invalid email address"
        isValid = False

    return isValid, error


def validate_register_data(data):
    error = ""
    isValid = True
    email_regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
    phone_number_regex = '^(\+\d{1,2}\s?)?1?\-?\.?\s?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$'
    if not data.get('email'):
        error = "Email field empty"
        isValid = False
    elif not data.get('name'):
        error = "Name field empty"
        isValid = False
    elif not data.get('password'):
        error = "Password field empty"
        isValid = False
    elif not data.get('phoneNumber'):
        error = "Phone number field empty"
        isValid = False
    elif not data.get('dob'):
        error = "DOB field empty"
        isValid = False
    elif not re.search(email_regex, data.get('email')):
        error = "Invalid email address"
        isValid = False
    elif not re.search(phone_number_regex, data.get('phoneNumber')):
        error = "Invalid phone number"
        isValid = False
    if not error:
        dob = datetime.strptime(data.get('dob'), '%d/%m/%Y').date()
        if not isinstance(dob, date):
            error = "Invalid DOB"
            isValid = False
    return isValid, error
