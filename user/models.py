from django.db import models
from django.conf import settings
from django.contrib.auth.models import PermissionsMixin, AbstractBaseUser, BaseUserManager, AbstractUser
from datetime import datetime


class User_manager(BaseUserManager):
    def create_user(self, name, email, password, phone_number, dob):
        email = self.normalize_email(email)
        user = self.model(name=name, email=email,
                          phone_number=phone_number, dob=dob)
        user.set_password(password)
        user.save(using=self.db)
        return user

    def create_superuser(self, name, email, password, phone_number, dob):
        user = self.create_user(
            name=name, email=email, password=password, phone_number=phone_number, dob=dob)
        return user


class User(AbstractBaseUser):
    name = models.CharField(max_length=32, unique=False)
    email = models.EmailField(max_length=32, unique=True)
    phone_number = models.CharField(max_length=20, unique=True)
    dob = models.DateField()

    REQUIRED_FIELDS = ["name", "phone_number", "dob"]
    USERNAME_FIELD = "email"
    objects = User_manager()

    def __str__(self):
        return self.email
    class Meta:
        db_table = 'users'
