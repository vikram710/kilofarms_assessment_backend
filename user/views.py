from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import update_last_login
from .models import *
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from django.conf import settings
import json
import jwt
import time
from .helpers import validate_login_data, validate_register_data


@require_http_methods(["GET"])
def login(request):
    if request.method == "GET":
        responseBody = {}
        isValid, error = validate_login_data(request.GET)
        if not isValid:
            responseBody['status_code'] = 400
            responseBody['message'] = error
            return JsonResponse(responseBody)
        try:
            user = User.objects.get(email=request.GET.get('email'))
            if user:
                auth_user = authenticate(
                    email=request.GET.get('email'), password=request.GET.get('password'))
                if auth_user:
                    payload = {
                        'id': user.id,
                        'email': user.email,
                        'time': time.time()
                    }
                    encoded = jwt.encode(
                        payload, settings.JWT_SECRET_KEY).decode('utf-8')
                    jwt_token = {'apiToken': encoded, 'id': user.id}
                    responseBody['status_code'] = 200
                    responseBody['message'] = jwt_token
                    update_last_login(None, user)
                    user.save()
                else:
                    responseBody['status_code'] = 400
                    responseBody['message'] = "Wrong credentials"
        except User.DoesNotExist:
            user = None
            responseBody['status_code'] = 400
            responseBody['message'] = "User does not exist"
        except Exception as e:
            responseBody['status_code'] = 500
            responseBody['message'] = "Internal Server Error"

        return JsonResponse(responseBody)


@require_http_methods(["POST"])
def register(request):
    if request.method == 'POST':
        responseBody = {}
        requestBody = json.loads(request.body)
        isValid, error = validate_register_data(requestBody)
        if not isValid:
            responseBody['status_code'] = 400
            responseBody['message'] = error
            return JsonResponse(responseBody)
        try:
            emailExists = User.objects.filter(
                email=requestBody['email']).first()
            phnoExists = User.objects.filter(
                phone_number=requestBody['phoneNumber']).first()

            if not emailExists and not phnoExists:
                dob = datetime.strptime(requestBody['dob'], '%d/%m/%Y').date()
                User.objects.create_user(
                    requestBody['name'], requestBody['email'], requestBody['password'], requestBody['phoneNumber'], dob
                )
                responseBody['status_code'] = 200
                responseBody['message'] = 'Account successfully created'

            else:
                if emailExists:
                    responseBody['status_code'] = 400
                    responseBody['message'] = 'Email must be unique'
                else:
                    responseBody['status_code'] = 400
                    responseBody['message'] = 'Phone number must be unique'
        except Exception as e:
            responseBody['status_code'] = 500
            responseBody['message'] = "Internal Server Error"

        return JsonResponse(responseBody)
